<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<meta charset="utf-8">
<title>Asia Pacific University of Technology and Innovation</title>

<style>
@charset "utf-8";
/* CSS Document */

body 
{
	margin:0px;
	padding:0px;
}


img
{	
	max-width:100%;
}

.logo
{	
	display:block;
	margin-left:auto;
	margin-right:auto;
}

.content-center
{
	display:block;
	margin-left:3%;
	margin-right:3%;
}

.text-body
{
	color:#333333;
	font-family:Arial, Helvetica, sans-serif;
	font-size:16px;
}	
	
.text-red
{
	color:#FF0000;
	font-family:Arial, Helvetica, sans-serif;
	font-size:16px;
}

.text-sm
{
	color:#333333;
	font-family:Arial, Helvetica, sans-serif;
	font-size:14px;
}

.img-inline
{
	display:inline-block;
}

a:link,a:visited
{
	color:#0563c1;
	text-decoration:none;
}

.brochures
{
	padding:8px;
}

li{
   color:#333333;
   list-style:square;
}

</style>
</head>

<body>
<!-------------------------------------------------------------------------------------------------------------->
<!----logo-changes---------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------------------------->
<center><img class="logo" src="http://www.apu.edu.my/sites/default/files/2014_APU-APIIT-Merge-Logo_FINAL-01-960.jpg"/></center>
<br><br>
<!-------------------------------------------------------------------------------------------------------------->
<!----end-logo-changes------------------------------------------------------------------------------------------>
<!-------------------------------------------------------------------------------------------------------------->



<div class="content-center">
<p>
  <!-------------------------------------------------------------------------------------------------------------->
  <!----content-1------------------------------------------------------------------------------------------------->
  <!-------------------------------------------------------------------------------------------------------------->
  <span class="text-body">
    Dear <strong>{NAME},</strong>
  <br><br>
    Greetings from Asia Pacific University of Technology & Innovation (APU), Malaysia.
  <br><br>
    The <strong>Asia Pacific University of Technology & Innovation (APU) </strong>is amongst Malaysia’s Premier Private Universities, and is where a unique fusion of technology, innovation and creativity works effectively towards preparing professional graduates for significant roles in business and society globally. APU has earned an enviable reputation as an award-winning University through its achievements in winning a host of prestigious awards at national and international levels.
  <br><br>
  <!-------------------------------------------------------------------------------------------------------------->
  <!----picture-iconic-------------------------------------------------------------------------------------------->
  <!-------------------------------------------------------------------------------------------------------------->
  <img style="width: 960px;" src="https://www.apu.edu.my/sites/default/files/2024-campus-banner_email960_0.jpg"/>
  <br><br>
  <!-------------------------------------------------------------------------------------------------------------->
  <!----end-picture-iconic---------------------------------------------------------------------------------------->
  <!-------------------------------------------------------------------------------------------------------------->
  <strong><u>Malaysia’s Highest Rated Emerging Universities</u><br>
  </strong></span><span class="text-body"><strong><br>
  </strong></span><span class="text-body">APU was announced as among the <strong>Highest Rated Emerging Universities in Malaysia</strong>, being rated 5-STAR (EXCELLENT Rating) under the latest SETARA Ratings by the Ministry of Higher Education (MOHE). APU has maintained this Excellent Rating consecutively in the SETARA 2011, 2013, 2017 as well as in the latest ratings announced on 18th Dec 2020. The SETARA ratings system measures the performance of teaching and learning in universities in Malaysia.</span></p>
<p><span class="text-body"><strong><u>APU Makes Waves in the QS World University Ranking 2024</u></strong><br>
  <br>
  <strong>Asia Pacific University of Technology &amp; Innovation (APU)</strong> is the<strong> ONLY Malaysian University to achieve the double distinction</strong> of achieving the <strong>QS “5 Stars Plus” Rating</strong> as well as being <strong>Ranked in the QS World University Ranking 2024, where APU is ranked in the Top 2.2% in the World</strong>.</span></p>
<p><span class="text-body">Facts regarding APU’s achievements in the latest QS World University rankings:<br>
    <br>
    • <strong>Ranked #621-630 in the World</strong><br>
    • <strong>Ranked No. 179 in Asia</strong><br>
    • <strong>Ranked No.1 for International Students in Malaysia</strong><br>
    • <strong>Ranked No.16 in the World for International Students</strong><br>
    • <strong>Ranked Top 200 for International Faculty in the World</strong><br>
<br>
  <strong>QS STARS - APU</strong> is the <strong>First Malaysian University</strong> to achieve an overall rating of <strong>Five Stars Plus in the latest QS Stars Rating</strong> awards that were presented at the QS Apple Conference on 1st Nov 2021. Five Stars Plus institution must achieve five stars across all categories in addition to achieving minimum highest benchmark score by QS STARS. <strong>APU is amongst 20 universities worldwide to achieve this honour</strong>.<br>
  <br>
  <strong><u>APU is an "Premier Digital Tech University" - MDEC</u></strong><br>
  <br>
  APU was awarded the Premier Digital Tech University status by the Malaysia Digital Economy Corporation (MDEC). APU is recognised for its commitment in offering top-notch digital technology courses and ensuring our highly-skilled graduates continue to flourish and fill future digital job demands locally and globally. Premier Digital Tech University status was awarded by the Minister of Higher Education Malaysia.<br><br>
  Originally established as the Asia Pacific Institute of Information Technology (APIIT) in 1993 and Asia Pacific University College of Technology & Innovation (UCTI) in 2004, APU’s sound approach to nurturing school leavers into qualified professionals has resulted in our graduates being highly sought after by employers.<br><br>
  </span>
  <!-------------------------------------------------------------------------------------------------------------->
  <!----end-content-1--------------------------------------------------------------------------------------------->
  <!-------------------------------------------------------------------------------------------------------------->
  
  
  
  <!-------------------------------------------------------------------------------------------------------------->
  <!----why-APU--------------------------------------------------------------------------------------------------->
  <!-------------------------------------------------------------------------------------------------------------->
  <span class="text-body">
    <strong><u>Why Asia Pacific University (APU)?</u></strong></span><br>
  <!-------------------------------------------------------------------------------------------------------------->
  <!----end-why-APU----------------------------------------------------------------------------------------------->
  <!-------------------------------------------------------------------------------------------------------------->
  
  
  
  <!-------------------------------------------------------------------------------------------------------------->
  <!----picture-why-apu------------------------------------------------------------------------------------------->
  <!--------------------------------------------------------------------------------------------------------------></p>
<div class="img-inline">
  <img style="width: 1050px" src="https://www.apu.edu.my/sites/default/files/2023_usp-for-website-1050.jpg"/>
</div>
	<br>
	<br>
  <!-------------------------------------------------------------------------------------------------------------->
  <!----end-picture-why-apu--------------------------------------------------------------------------------------->
  <!-------------------------------------------------------------------------------------------------------------->
  
  
  
  <!-------------------------------------------------------------------------------------------------------------->
  <!----content-2------------------------------------------------------------------------------------------------->
  <!-------------------------------------------------------------------------------------------------------------->
  <span class="text-red">
    <strong>A Truly International Community - Students from over 130 Countries </strong>
    <br><br>
  </span>
  
  <span class="text-body">
    With an international student community of more than 130 countries studying in its Malaysian campus, APU offers a truly cosmopolitan learning environment which prepares students well for the global challenges which lie ahead. APU offers a wide range of degrees with Technology as a common core. It is APU’s aim to nurture and encourage innovation through our programmes, with the intention of producing individuals who will learn, adapt and think differently in new and better ways.
    <br><br>
    <strong><u>PROGRAMMES OFFERED @ ASIA PACIFIC UNIVERSITY (APU)</u></strong>
    <br>
    <br>
    At APU, our programmes are developed to provide you not only with interesting and stimulating modules to develop your mind, but also to enhance your knowledge and skills and increase your ability to compete for that dream job. You may find a listing of all the programmes at  <a href="http://www.apu.edu.my/our-courses" target="_blank"><strong>http://www.apu.edu.my/our-courses</strong></a> or in the Course Brochures below:
    <br><br>
  </span>
  <!-------------------------------------------------------------------------------------------------------------->
  <!----end-content-2--------------------------------------------------------------------------------------------->
  <!-------------------------------------------------------------------------------------------------------------->
  
  
  
  <!-------------------------------------------------------------------------------------------------------------->
  <!----picture-brochures----------------------------------------------------------------------------------------->
  <!--------------------------------------------------------------------------------------------------------------></p>
<div class="img-inline">
  <a href="https://www.apu.edu.my/sites/default/files/e2024jan_pre-u_brochure.pdf" target="_blank"><img class="brochures" src="https://www.apu.edu.my/sites/default/files/download_ebrochure_preu_0.jpg"/></a> <a href="https://www.apu.edu.my/sites/default/files/e2024may_business.pdf" target="_blank"><img class="brochures" src="https://www.apu.edu.my/sites/default/files/download_ebrochure_business_0.jpg"/></a>
</div>
<br><br>
<div class="img-inline"> <a href="https://www.apu.edu.my/sites/default/files/e2024may_computing.pdf" target="_blank"><img class="brochures" src="https://www.apu.edu.my/sites/default/files/download_ebrochure_computing_0.jpg"/></a> <a href="https://www.apu.edu.my/sites/default/files/e2024may_engineering.pdf" target="_blank"><img class="brochures" src="https://www.apu.edu.my/sites/default/files/download_ebrochure_engineering_0.jpg"/></a>
</div>
<br><br>
<div class="img-inline">
<a href="https://www.apu.edu.my/sites/default/files/e2024may_accounting.pdf" target="_blank"><img class="brochures" src="https://www.apu.edu.my/sites/default/files/download_ebrochure_accounting_0.jpg"/></a> <a href="https://www.apu.edu.my/sites/default/files/e2024_designmedia.pdf" target="_blank"><img class="brochures" src="https://www.apu.edu.my/sites/default/files/download_ebrochure_design_0.jpg"/></a></div>
<br><br>
<div class="img-inline"><a href="https://www.apu.edu.my/sites/default/files/e2024may_mip.pdf" target="_blank"><img class="brochures" src="https://www.apu.edu.my/sites/default/files/download_ebrochure_media_0.jpg"/></a> <a href="https://www.apu.edu.my/sites/default/files/2024jan_pg_brochure-min.pdf" target="_blank"><img class="brochures" src="https://www.apu.edu.my/sites/default/files/download_ebrochure_postgraduate_0_0.jpg"/></a></div>
<br><br>
	<div class="img-inline"><a href="https://www.apu.edu.my/sites/default/files/e2024_english_leaflet-4.5.pdf" target="_blank"><img class="brochures" src="https://www.apu.edu.my/sites/default/files/download_ebrochure_aplc_0.jpg"/></a></div>
<br><br>
<!-------------------------------------------------------------------------------------------------------------->
<!----end-picture-brochures------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------------------------->



<!-------------------------------------------------------------------------------------------------------------->
<!----content-3------------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------------------------->
<span class="text-body">
Just to give you a quick update on the Intake Dates and Admission Procedures:
<!--br><br>
<strong><u>FOUNDATION & DIPLOMA INTAKES:</u></strong>

{INTAKE_DATES_2}

<br>
<strong><u>BACHELOR DEGREE INTAKES:</u></strong>

{INTAKE_DATES}
<br-->
<br><br>
<strong><u>FOUNDATION, DIPLOMA & BACHELOR DEGREE INTAKES:</u></strong>
{INTAKE_DATES}
For more details on intake dates, please visit <a href="https://www.apu.edu.my/study-apu/intake-calendar" target="_blank">https://www.apu.edu.my/study-apu/intake-calendar</a>
<br>
<br>
<!-------------------------------------------------------------------------------------------------------------->

<strong><u>APPLICATION PROCEDURE:</u></strong>
<br><br>
In order to submit an application, it would be very useful if you could send copies of your <strong>high school results and other academic qualifications to me</strong>. This will allow our academic team to evaluate these before we advise you on how to proceed.
<br><br>
The following are some documents/forms to assist you in your application: 
<ul>
<li><a href="https://www.apu.edu.my/sites/default/files/apu_feeguide_intug_2024.pdf" target="_blank">APU Undergraduate Fees Guide</a></li>
<li><a href="https://www.apu.edu.my/sites/default/files/e2024_apu_residenceform.pdf" target="_blank">APU Accommodation Form</a></li>
</ul>
With an international student community from more than 130 countries studying in its Malaysian campus, APU offers a truly cosmopolitan learning environment which prepares students well for the global challenges which lie ahead. APU now has over 6,000 international students. 
<br><br>
Please feel free to visit us or contact us and we look forward to your response. 
<br><br>
We do look forward to hearing from you.
<br><br>
Warm Regards,
<br><br>
</span>
<!------------------------------------------------------------------------------------------------------------ -->
<!----end-content-3------------------------------------------------------------------------------------------- -->

<table>
<tr>
<td><img src="http://www.apu.edu.my/sites/all/themes/APU/images/email/logo.jpg" align="top"></td>

<td>
<span class="text-body">
<strong>{SENDER_NAME}</strong>
<br>
</span>
<span class="text-body">
<strong>Asia Pacific University of Technology & Innovation (APU)</strong>
<br>
<strong>Asia Pacific Institute of Information Technology (APIIT)</strong>
<br>
</span>
<span class="text-sm">
Technology Park Malaysia,
<br>
Bukit Jalil, 57000 Kuala Lumpur, Malaysia
<br>
Tel: +603 8996 1000 
<br>
Email: <a href="#">{SENDER_EMAIL}</a> | Web: <a href="www.apu.edu.my" target="_blank">www.apu.edu.my</a> | Facebook: <a href="www.facebook.com/apuniversity" target="_blank">www.facebook.com/apuniversity </a>
<br><br>
</span>
</td>
</tr>
</table>
<!-------------------------------------------------------------------------------------------------------------->
<!----end-footer-changes---------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------------------------->
</div>
</body>
</html>